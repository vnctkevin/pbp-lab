from django.db import models

# Create your models here.
class Note(models.Model):
    to = models.CharField(max_length=50)
    fromNote = models.CharField(max_length=50)
    title = models.CharField(max_length=100)
    message = models.TextField()
