# import form class from django
from django.utils.translation import gettext_lazy as _
from django import forms
from django.forms import widgets
from .models import Friend

# https://stackoverflow.com/questions/3367091/whats-the-cleanest-simplest-to-get-running-datepicker-in-django


class DateInput(forms.DateInput):
    input_type = 'date'

# FriendForm class for views
class FriendForm(forms.ModelForm):
    class Meta:
        model = Friend
        fields=['name','npm','dob']

        name = forms.CharField(max_length='30')
        npm = forms.CharField(max_length='10')
        dob = forms.DateField()

        labels = {
            'name': _('Full Name'),
            'npm': _('NPM'),
            'dob': _('Date of Birth'),
        }

        widgets = {
            'dob': DateInput(),
        }
