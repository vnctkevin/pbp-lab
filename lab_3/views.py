from django.http.response import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from .models import Friend
from lab_3.forms import FriendForm

# imported login required
from django.contrib.auth.decorators import login_required

# For each, added login required so must be logged in as admin
@login_required(login_url='/admin/login/')
def index(request):
    friends = Friend.objects.all().values() 
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)

@login_required(login_url='/admin/login/')
def add_friend(request):
  
    # create object of form
    form = FriendForm(request.POST or None)
      
    # check if form data is valid
    if (form.is_valid() and request.method == 'POST'):
        # save the form data to model
        form.save()
        # when saved go back to lab-3
        return HttpResponseRedirect('/lab-3')
    
    else:
        form = FriendForm()

    return render(request, 'lab3_form.html', {'form': form})
