# import form class from django
from django.utils.translation import gettext_lazy as _
from django import forms
from django.forms import widgets
from .models import Note

# https://stackoverflow.com/questions/3367091/whats-the-cleanest-simplest-to-get-running-datepicker-in-django


# NoteForm class for views
class NoteForm(forms.ModelForm):
    class Meta:
        model = Note
        fields=['to','fromNote','title', 'message']

        to = forms.CharField(max_length=50, required=True)
        fromNote = forms.CharField(max_length=50, required=True)
        title = forms.CharField(max_length=100, required=True)
        message = forms.Textarea()

        labels = {
            'to': _('To'),
            'fromNote': _('From'),
            'title': _('Message Title'),
            'message': _('Message'),
        }
        widgets = {
            'message': forms.Textarea(),
        }

        error_messages = {
		    'required' : 'Input cannot be empty',
	    }


