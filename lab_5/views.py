from django.http.response import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from .models import Note
from lab_4.forms import NoteForm

# imported login required
from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required(login_url='/admin/login/')
def index(request):
    note = Note.objects.all().values() 
    response = {'note': note}
    return render(request, 'lab4_index.html', response)