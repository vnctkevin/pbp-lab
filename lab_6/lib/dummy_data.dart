import 'package:lab_6/models/pojok_curhat.dart';
import 'package:lab_6/models/pojok_curhat_json.dart';

// ignore: non_constant_identifier_names
Pojok_CurhatJson DUMMY_CATEGORIES = Pojok_CurhatJson(
  model: 'Pojok_Curhat.Pojok_Curhat',
  pk: 1,
  fields: [
    Pojok_Curhat(
      id: "Bruce W.",
      title: 'Aku sayang kesehatan mentalku :(',
      message: 'pekerjaan,teman,pendidikan',
    ),
    Pojok_Curhat(
      id: "Wumbo",
      title: 'Capek aku hidup',
      message: 'Aku benci diriku sendiri aku bingung aku capek aku ga mau gini',
    ),
    Pojok_Curhat(
      id: "Hitan",
      title: 'Capek',
      message: 'Lelah lelah lelaaaaaahhhhh',
    )
  ],
);
