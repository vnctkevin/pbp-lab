# JAWABAN PERTANYAAN LAB 2
## Oleh Kevin/2006483795/PBP C

1. Apakah perbedaan antara JSON dan XML?

JSON (JavaScript Object Notation) adalah format pertukaran data berbasis teks yang digunakan untuk menampilkan objek. Ini adalah format yang berdasarkan pada bahasa pemrograman JavaScript (JS). XML (Extensive Markup Language) adalah bahasa markup yang digunakan untuk menyimpan dan mengangkut data melalui jaringan. XML dapat dimanfaatkan untuk menentukan elemen markup dan menghasilkan bahasa markup yang disesuaikan. Unit dasar dalam XML dikenal sebagai elemen. 

Beberapa perbedaan lain antara JSON dan XML antara lain:
* Object pada JSON punya tipe sedangkan di XML data itu _typeless_
* JSON tidak memiliki namespace support sedangkan XML punya.
* JSON tidak dapat menampilkan data dimana XML bisa menampilkan data.
* JSON kurang aman dibandingkan XML
* JSON hanya support UTF-8 encoding sedangkan XML mampu mensupport berbagai macam format encoding lain.

2. Apakah perbedaan antara HTML dan XML?

HTML (HyperText Markup Language) digunakan untuk membuat halaman web dan aplikasi web. Ini adalah bahasa markup yang digunakan untuk menerapkan tata letak dan konvensi pemformatan ke dokumen teks. Bahasa ini terutama digunakan untuk menampilkan data, bukan untuk mentransfer data.

XML juga digunakan untuk membuat halaman dan aplikasi web. Namun, XML lebih dinamis karena digunakan untuk mentransfer data, bukan untuk menampilkan data. Inilah perbedaan mendasar antara HTML dan XML, walaupun keduanya sama-sama _markup language_. 


Reference:
https://www.guru99.com/json-vs-xml-difference.html
https://www.monitorteknologi.com/perbedaan-json-dan-xml/
https://blogs.masterweb.com/perbedaan-xml-dan-html/
https://www.geeksforgeeks.org/html-vs-xml/
